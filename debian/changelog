asciiart (0.3.0-1) UNRELEASED; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on ruby-rainbow and ruby-rmagick.
  * Bump debhelper from old 12 to 13.

  [ Rajesh Simandalahi ]
  * New upstream release.
  * Update and refresh patch.

  [ Debian Janitor ]
  * Update standards version to 4.6.1, no changes needed.

  [ mprio_zip ]
  * New upstream version 0.3.0
  * Uptade the standard version

 -- mprio_zip <marianapires00@gmail.com>  Sun, 03 Dec 2023 17:10:13 -0300

asciiart (0.0.10-2) unstable; urgency=medium

  * Team upload
  * Add simple autopkgtest
  * Update "Loosen dependencies" patch to accept rmagick >= 2.13

 -- Antonio Terceiro <terceiro@debian.org>  Tue, 12 Oct 2021 07:05:05 -0300

asciiart (0.0.10-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * d/control (Standards-Version): Bump to 4.6.0.
    (Depends): Fix ruby-script-but-no-ruby-dep.
  * d/copyright (Copyright): Update team's copyright.
  * d/patches/0001-Loosen-dependencies.patch: Adjust patch.
  * d/patches/0002-Fix-rmagick-warning.patch: Drop patch applied upstream.
  * d/patches/series: Adjust series file.
  * d/upstream/metadata (Bug-Submit): Fix URL.

 -- Daniel Leidert <dleidert@debian.org>  Thu, 07 Oct 2021 08:42:11 +0200

asciiart (0.0.9-2) unstable; urgency=medium

  * Team upload.
  * d/compat: Remove obsolete file.
  * d/control: Add Rules-Requires-Root field.
    (Build-Depends): Use debhelper-compat 12.
    (Standards-Version): Bump to 4.5.0.
    (Vcs-Git, Vcs-Browser): Use salsa URLs.
    (Depends): Remove ruby-interpreter and use ${ruby:Depends}.
  * d/copyright: Add Upstream-Contact field.
    (Format): Fix insecure-copyright-format-uri.
    (Copyright): Add team.
  * d/rules: Run dependency checker and use gem installation layout.
  * d/watch: Use gemwatch URL.
  * d/patches/0001-Loosen-dependencies.patch: Add patch.
    - Loosen dependencies because their versions are not longer available.
  * d/patches/0002-Fix-rmagick-warning.patch: Add patch.
    - Fix a warning by recent rmagick.
  * d/patches/series: Enable new patch.
  * d/upstream/metadata: Add upstream metadata.

 -- Daniel Leidert <dleidert@debian.org>  Thu, 16 Apr 2020 23:03:22 +0200

asciiart (0.0.9-1) unstable; urgency=medium

  * Initial release (Closes: #805548)

 -- Balasankar C <balasankarc@autistici.org>  Thu, 19 Nov 2015 16:05:57 +0530
